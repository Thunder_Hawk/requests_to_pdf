"""
    Delete images after use
"""


from os import listdir, remove, path


FOLDER_PATH = "/home/toto/Desktop/test"


def delete_jpg_files(folder_path):
    """
    Delete all images

    Args:
        folder_path (_str_): relative path
    """
    for file in listdir(folder_path):
        if file.endswith(".jpg"):
            remove(path.join(folder_path, file))
            print(f"Deleted: {file}")


if __name__ == "__main__":
    delete_jpg_files(FOLDER_PATH)
    print("Ended !")
