"""
    Set images to PDF file
"""

from os import listdir, path
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas


IMAGE_DIR = "/home/toto/Desktop/test"
OUTPUT_FILE = "magical_book.pdf"


def sort_images_by_name(image_dir):
    """
    Sort images

    Args:
        image_dir (_str_): relative path

    Returns:
        _list_: ordered name
    """
    images = [
        img
        for img in listdir(image_dir)
        if img.endswith(".jpg") or img.endswith(".png")
    ]
    return sorted(images)


def create_pdf(images, output_file):
    """
    Create PDF file

    Args:
        images (_list_): name sorted
        output_file (_str_): PDF file path
    """
    canva = canvas.Canvas(output_file, pagesize=letter)

    for image in images:
        image_path = path.join(IMAGE_DIR, image)
        canva.drawImage(
            image_path, 0, 0, width=canva._pagesize[0], height=canva._pagesize[1]
            # No getters in the lib
        )

        canva.showPage()

    canva.save()


if __name__ == "__main__":

    sorted_images = sort_images_by_name(IMAGE_DIR)
    create_pdf(sorted_images, OUTPUT_FILE)
    print("BOOM BOOM BOOM ! PDF creation complete.")
