 
# Pour Ben

## Mise en place de l'environnement 

#### Installer Python
> https://www.python.org/

#### puis :

```
git clone https://gitlab.com/Thunder_Hawk/requests_to_pdf.git

pip install -r .\Requirements.txt
```


## Fonctionnement

- Remplir à l'aide d'un copié collé les liens dans `urls.txt`

- Lancer le `dl_and_sort.py` et attendre

- Suivre les intructions puis lancer le `pdf.py`

- Supprimer les images à l'aide de `rm.py`


