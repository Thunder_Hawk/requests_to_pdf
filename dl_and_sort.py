"""
    Code for Ben !
"""

from os import rename
from urllib.request import urlretrieve
from time import sleep


INPUT_FILE = "urls.txt"
OUTPUT_FILE = "url.txt"
PAGES = int(input("int: number of pages ?"))


def replace_spaces_with_newlines(input_file, output_file):
    """
    Sort urls pastes in a file in an other

    Args:
        input_file (_str_): relative path of pasted urls file
        output_file (_str_): relative path of usable links
    """
    with open(input_file, "r", encoding="utf_8") as file:
        content = file.read()

    modified_content = content.replace(" ", "\n")

    with open(output_file, "w", encoding="utf_8") as file:
        file.write(modified_content)


def download_images(urls_file):
    """
    Download images from HTTP Requests

    Args:
        urls_file (_str_): relative path of urls file
    """
    with open(urls_file, "r", encoding="utf_8") as file:
        for i, url in enumerate(file, start=1):
            url = url.strip()
            filename = f"{i}.jpg"
            urlretrieve(url, filename)
            print(f"file_{i} downloaded")
            sleep(0.5)


def rename_file(old_name, new_name):
    """
    Rename file

    Args:
        old_name (_str_): filename
        new_name (_str_): targeted filename
    """
    try:
        rename(old_name, new_name)
        print(f"File '{old_name}' has been renamed to '{new_name}'.")
    except FileNotFoundError:
        print(f"Error: File '{old_name}' not found.")


def iterative_rename(pages):
    """
    Sort files in great order (1;3;2;5;4;...)
    Args:
        pages (_int_): number of pages
    """
    base_value = 1
    rename_file(f"{1}.jpg", f"{0}*.jpg")
    for i in range(3, pages + 1, 2):
        rename_file(f"{i}.jpg", f"{base_value}*.jpg")
        base_value += 1
        rename_file(f"{i-1}.jpg", f"{base_value}*.jpg")
        base_value += 1


if __name__ == "__main__":

    replace_spaces_with_newlines(INPUT_FILE, OUTPUT_FILE)
    download_images(OUTPUT_FILE)
    iterative_rename(PAGES)
    print("Check in images on the folder (maybe duplicated pages, like page 4)")
    print("\nGo to https://tools.pdf24.org/en/images-to-pdf to create your pdf file or use pdf.py !")
